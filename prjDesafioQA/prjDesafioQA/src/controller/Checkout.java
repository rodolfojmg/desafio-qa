package controller;

import java.util.ArrayList;
import java.util.List;

import model.Produto;

public class Checkout {

	private List<Produto> produtos;

	/**
	 * M�todo Construtor
	 */
	public Checkout() {
		produtos = new ArrayList<Produto>();
	}

	/***
	 * INSTANCIA A LISTA DE PRODUTOS ATRAVES DOS ITENS
	 * 
	 * @param itensProdutos
	 */
	public void scan(String itensProdutos) {

		Produto produto;

		for (int i = 0; i < itensProdutos.length(); i++) {

			produto = new Produto();

			switch (itensProdutos.charAt(i)) {

			case 'A':
				produto.setItem('A');
				produtos.add(produto);
				break;

			case 'B':
				produto.setItem('B');
				produtos.add(produto);
				break;

			case 'C':
				produto.setItem('C');
				produtos.add(produto);
				break;

			case 'D':
				produto.setItem('D');
				produtos.add(produto);
				break;

			default:
				break;
			}
		}
	}

	/***
	 * CALCULA A REGRA DE NEGOCIO DOS PRODUTOS
	 * 
	 * @return
	 */
	public int total() {

		int totalProdA = 0;
		int totalProdB = 0;
		int valor = 0;

		for (Produto produto : produtos) {

			switch (produto.getItem()) {

			case 'A':
				valor += 50;
				totalProdA += 1;
				if (totalProdA == 3) {
					totalProdA = 0;
					valor -= 20;
				}
				break;

			case 'B':
				valor += 30;
				totalProdB += 1;
				if (totalProdB == 2) {
					totalProdB = 0;
					valor -= 15;
				}
				break;

			case 'C':
				valor += 20;
				break;

			case 'D':
				valor += 15;
				break;

			default:
				break;
			}

		}

		return valor;
	}

}
